$('#header__carousel').carousel({
    interval: 4000
  })

  $('#services__carousel').carousel({
    interval: false 
  })

  

  var latestBtn = document.querySelectorAll('.collection__latest-btn');
  var popularBtn = document.querySelectorAll('.collection__popular-btn');
  var slideLatest = document.querySelectorAll('.collection__slider-latest');
  var slidePopular = document.querySelectorAll('.collection__slider-popular');

  function changeLatest() {
      for(i=0;i<slidePopular.length;i++) {
    slidePopular[i].classList.add('unvisible');
    slideLatest[i].classList.remove('unvisible');
    latestBtn[i].classList.add('gradient');
    popularBtn[i].classList.remove('gradient');
  }
  }

  function changePopular() {
    for(i=0;i<slidePopular.length;i++) {
    slidePopular[i].classList.remove('unvisible');
    slideLatest[i].classList.add('unvisible');
    latestBtn[i].classList.remove('gradient');
    popularBtn[i].classList.add('gradient');
  }
  }
  latestBtn[0].addEventListener('click',changeLatest);
  popularBtn[0].addEventListener('click',changePopular);